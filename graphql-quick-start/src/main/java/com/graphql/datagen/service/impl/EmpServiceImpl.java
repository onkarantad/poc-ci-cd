package com.graphql.datagen.service.impl;

import com.graphql.datagen.model.Employee;
import com.graphql.datagen.repo.EmpRepo;
import com.graphql.datagen.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmpServiceImpl implements EmpService {

    @Autowired
    EmpRepo empRepo;

    @Override
    public Employee updateEmployee(Employee emp) {
        return this.empRepo.save(emp);
    }

    @Override
    public Boolean removeEmployee(Integer empId) {
        Optional<Employee> emp = this.empRepo.findById(empId);
        emp.ifPresent(e -> this.empRepo.delete(e));
        return emp.isPresent();
    }

    @Override
    public List<Employee> getAll() {
        return this.empRepo.findAll();
    }

    @Override
    public Optional<Employee> get(int empId) {
        return this.empRepo.findById(empId);
    }
}
