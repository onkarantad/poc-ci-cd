package com.graphql.datagen.service;

import com.graphql.datagen.model.Employee;

import java.util.List;
import java.util.Optional;

public interface EmpService {
    Employee updateEmployee(Employee emp);
    Boolean removeEmployee(Integer empId);
    List<Employee> getAll();
    Optional<Employee> get(int empId);
}
