package com.graphql.datagen.controller;

import com.graphql.datagen.model.Employee;
import com.graphql.datagen.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.Optional;

@Controller
public class EmpController {

    @Autowired
    private EmpService empService;

    @MutationMapping("updateEmployee")
    public Employee update(@Argument(name = "inputData") Employee employee) {
        return this.empService.updateEmployee(employee);
    }

    @QueryMapping("getEmployee")
    public Optional<Employee> get(@Argument Integer empId) {
        return this.empService.get(empId);
    }

    @QueryMapping("allEmployee")
    public List<Employee> getAll() {
        return this.empService.getAll();
    }

    @MutationMapping("removeEmployee")
    public Boolean remove(@Argument Integer empId) {
        return this.empService.removeEmployee(empId);
    }

}
