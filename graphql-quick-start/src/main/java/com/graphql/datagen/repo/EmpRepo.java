package com.graphql.datagen.repo;

import com.graphql.datagen.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.graphql.data.GraphQlRepository;

@GraphQlRepository
public interface EmpRepo extends JpaRepository<Employee, Integer> {
}
